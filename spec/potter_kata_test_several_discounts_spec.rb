require 'potter_kata.rb'

describe 'PotterKata#TestSeveralDiscounts' do
  let(:potter_kata) { PotterKata.new }
  
  it 'gets 2 types of books' do
    expect(potter_kata.price([0, 0, 1])).to eq(8 + (8 * 2 * 0.95))
  end

  it 'gets 2 types of books' do
    expect(potter_kata.price([0, 0, 1, 1])).to eq(2 * (8 * 2 * 0.95))
  end

  it 'gets 4 types of books' do
    expect(potter_kata.price([0, 0, 1, 2, 2, 3])).to eq((8 * 4 * 0.8) + (8 * 2 * 0.95))
  end
  
  it 'gets 4 types of books' do
    expect(potter_kata.price([0, 1, 1, 2, 3, 4])).to eq(8 + (8 * 5 * 0.75))
  end
end

