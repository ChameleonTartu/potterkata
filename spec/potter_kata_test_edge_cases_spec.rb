require 'potter_kata.rb'

describe 'KataPotter#TestEdgeCases' do
  let(:potter_kata) { PotterKata.new }
  let(:few_books) { [0, 0, 1, 1, 2, 2, 3, 4] }
 

  let(:some_books) do
    [0, 0, 0,
     1, 1, 1,
     2, 2, 2,
     3, 3,
     4, 4]
  end
    
  let(:plenty_of_books) do
    [0, 0, 0, 0, 0, 
     1, 1, 1, 1, 1, 
     2, 2, 2, 2, 
     3, 3, 3, 3, 3, 
     4, 4, 4, 4] 
  end


  it 'gets 5 types of books' do
    expect(potter_kata.price(few_books)).to eq(2 * (8 * 4 * 0.8))
  end

  it 'gets 5 types of books' do
    expect(potter_kata.price(plenty_of_books)).to eq(3 * (8 * 5 * 0.75) + 2 * (8 * 4 * 0.8))
  end

  it 'gets 5 types of books' do
    expect(potter_kata.price(some_books)).to eq(2 * (8 * 4 * 0.8 ) + 8 * 5 * 0.75)
  end
end

