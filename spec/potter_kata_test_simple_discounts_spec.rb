require 'potter_kata.rb'

describe 'PotterKata#TestSimpleDiscounts' do
  let(:potter_kata) { PotterKata.new }
  it 'makes 2 books discount' do
    expect(potter_kata.price([0, 1])).to eq(8 * 2 * 0.95)
  end

  it 'makes 3 books discount' do
    expect(potter_kata.price([0, 2, 4])).to eq(8 * 3 * 0.9)
  end 

  it 'makes 4 books discount' do
    expect(potter_kata.price([0, 1, 2, 4])).to eq(8 * 4 * 0.8)
  end  

  it 'makes 5 books discount' do
    expect(potter_kata.price([0, 1, 2, 3, 4])).to eq(8 * 5 * 0.75)
  end
end

