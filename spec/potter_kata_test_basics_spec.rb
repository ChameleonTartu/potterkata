require 'potter_kata.rb'

describe 'PotterKata#TestBasics' do
  let(:potter_kata) { PotterKata.new }  

  it 'gets 0 books' do
    expect(potter_kata.price([])).to eq(0)
  end

  context 'with a book' do
    let(:price) { 8 }

    it 'gets a book' do
      expect(potter_kata.price([0])).to eq(price)
    end
    
    it 'gets a book' do
      expect(potter_kata.price([1])).to eq(price)
    end
    
    it 'gets a book' do
      expect(potter_kata.price([2])).to eq(price)
    end
    
    it 'gets a book' do
      expect(potter_kata.price([3])).to eq(price)
    end
    
    it 'gets a book' do
      expect(potter_kata.price([4])).to eq(price)
    end
  end

  it 'gets 2 books' do
    expect(potter_kata.price([0, 0])).to eq(16)
  end

  it 'gets 3 books' do
    expect(potter_kata.price([1, 1, 1])).to eq(24)
  end
end

