class PotterKata
  def initialize(price = 8, percent_of_initial_price = [1, 0.95, 0.9, 0.8, 0.75])
    @price = price
    @percent_of_initial_price = percent_of_initial_price 
  end

  def _convert_books(books) 
    @books = {}
    books.each do |book_type|
      if @books.key?(book_type)
        @books[book_type] = @books[book_type] + 1
      else
        @books[book_type] = 1
      end
    end
  end

  def _delete_layer
    @books.each do |key, value|
      @books[key] = value - 1
    end
    @books.delete_if do |key, value|
      value == 0
    end
  end

  def _greedy_discount
    if @books.size == 0
      0
    else
      total_cost = @books.size * @price * @percent_of_initial_price[@books.size - 1] 
      _delete_layer 
      total_cost + _greedy_discount
    end 
  end 

  def price(books)
    _convert_books(books)
    if books.size % @percent_of_initial_price.size == 3 && @books.size == @percent_of_initial_price.size  
      return (2 * @price * @percent_of_initial_price[3] + _greedy_discount - 
        @price * (@percent_of_initial_price[2] + @percent_of_initial_price[4])).round(3)
    end
    _greedy_discount
  end
end

